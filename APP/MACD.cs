﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;


namespace APP
{
    internal class MACD: Oscilador
    {
        protected double _EMAcorto; // Periodo de media movil corta [7, 14, 21, 50]
        protected double _EMAlargo; // Periodo de media movil largo [21, 50, 100, 200]
        protected double _lineaSenal;
        protected double _MACD = 0;

        public MACD(string nombre, string descripcion, int periodo, double valor, double EMACorto, double EMAlargo, double lineaSenal)
            : base(nombre, descripcion, periodo, valor)
        {
            _lineaSenal = lineaSenal;
            _EMAcorto = EMACorto;
            _EMAlargo = EMAlargo;
        }


        public double LineaSenal { 
            get { return _lineaSenal; } set { _lineaSenal = value; }
        }

        public double EmaCorto {
            set { _EMAcorto = value; }
        }

        public double EmaLargo
        {
            set { _EMAlargo = value; }
        }

        public override double Calcular(double precioCierreActual, double precioCierreAnterior)
        {
            // Calcula el precio valor de MACD para un conjunto de precios dados utilizando la formula de MACD
            // y los periodos de meias moviles corta y larga
            _MACD = (precioCierreActual + precioCierreAnterior) / (_EMAcorto - _EMAlargo);

            Console.WriteLine($"\tValor de precioCierreActual = {precioCierreActual}");
            Console.WriteLine($"\tValor de precioCierreAnterior = {precioCierreAnterior}");
            Console.WriteLine($"\tValor de EMAcorto = {_EMAcorto}");
            Console.WriteLine($"\tValor de EMAlargo = {_EMAlargo}");
            Console.WriteLine($"\tValor de MACD = {_MACD}");
            
            return _MACD;
        }

        public override string Interpretar()
        {
            // Proporciona una interpretacion el valor de MACD en relacion con la lindea de senal y posibles
            // cruces de a acuerdo a los criterios dados

            if (_lineaSenal > _MACD & Math.Abs(_lineaSenal - _MACD) < 10){
                return "Posible posicion larga.";
            }

            if (_lineaSenal < _MACD & Math.Abs(_MACD - _lineaSenal) < 10) {
                return "Posible posicion corta";
            }

            return "Sin condiciones para operar de acuerdo con MACD";
        }

    }
}
