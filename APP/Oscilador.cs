﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP
{
    internal abstract class Oscilador
    {
        protected string _nombre; // Nombre del oscilador
        protected string _descripcion; // Breve descripcion del oscilador y su funcionamiento
        protected int _periodo; // Periodo utilizado para el calculo del oscilador
        protected double _valor; // Valor actual del oscilador

        public Oscilador(string nombre, string descripcion, int periodo, double valor) {
            _nombre = nombre;
            _descripcion = descripcion;
            _periodo = periodo; 
            _valor = valor; 
        }

        public virtual double Calcular(double precioCierreActual, double precioCierreAnterior) {
            return _valor / (double)_periodo;
        }

        public virtual string Interpretar() {
            // Proporciona euna interpretacion del valor del oscilador en el mercado
            return "";
        }

    }
}
