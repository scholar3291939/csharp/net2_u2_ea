﻿
namespace APP {

    class Program { 

        static Random random = new Random();

        public static void Main(string[] args) {




            RSI indicadorRSI = new RSI("Indicador RSI", "Relative Strenght Index", 10, getValorActual());
            MACD indicadorMACD = new MACD("Indicador RSI", "Relative Strenght Index", 10, getValorActual(), getEMAcorto(), getEMAlargo(), getLineaSenal());

            double _precioCierreAnterior = precioCierreAnterior();
            double _precioCierreActual = precioCierreActual(_precioCierreAnterior);


            indicadorRSI.Calcular(_precioCierreActual, _precioCierreAnterior);
            string RSI = indicadorRSI.Interpretar();
            Console.WriteLine($"Indicador RSI {RSI}");


            indicadorMACD.Calcular(_precioCierreActual, _precioCierreAnterior);
            string MACD = indicadorMACD.Interpretar();
            Console.WriteLine($"Indicador MACD {MACD}");

        }



        public static int getEMAlargo() {
            int _index = (int)random.NextInt64(4);
            int[] EMAlargo = { 21, 50, 100, 200 };
            return EMAlargo[_index];
        }

        public static int getEMAcorto(){ 
            int _index = (int)random.NextInt64(4);
            int[] EMAcorto = {7,14,21,50 };
            return EMAcorto[_index];
        }

        public static double getValorActual() { 
            return random.NextDouble() * 1000;
        }


        public static double precioCierreAnterior()
        {
            return random.NextDouble() * 1000;

        }

        public static double precioCierreActual(double precioCierreAnterior) {

            double cambio = precioCierreAnterior * random.NextDouble(); 

            if (random.NextInt64(2).Equals(1))
            {
                return precioCierreAnterior + cambio; // El valor sube
            }
            return precioCierreAnterior - cambio; // El valor baja
        }

        public static double getLineaSenal() {
            return random.NextDouble();
        }

    }
}