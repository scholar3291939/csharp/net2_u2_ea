﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP
{
    // Relative Strength index
    internal class RSI : Oscilador
    {
        protected double _nivelSobreCompra; // Valor limite que indica una posible sobre compra
        protected double _nivelSobreVenta; // Valor limite que indica una posible sobre venta
        protected double _RSI = 0;

        public RSI(string nombre, string descripcion, int periodo, double valor) 
            : base (nombre, descripcion, periodo, valor)
        { 
            
        }

        public override double Calcular(double precioCierreActual, double precioCierreAnterior) {
            // Calcula el valor del RSI para un conjunto de precios dados usando la formula del RSI
            double RS = CalcularRS(precioCierreActual, precioCierreAnterior);
            _RSI = 100 - (100 / (1 + RS));

            Console.WriteLine($"\tValor de precioCierreActual = {precioCierreActual}");
            Console.WriteLine($"\tValor de precioCierreAnterior = {precioCierreAnterior}");
            Console.WriteLine($"\tValor de RSI = {_RSI}");

            return _RSI;
        }


        protected double CalcularRS(double precioCierreActual, double precioCierreAnterior) { 
            // Calcula el valor del RS en funcion de los precios previos y actuales
            double diferencia = precioCierreActual - precioCierreAnterior;
            if (diferencia.Equals(0)) {
                _RSI = 0;
            }

            if (diferencia > 0) {
                _RSI = 101;
            }

            else {
                _RSI  = - 0.1;
            }

            return _RSI;
        }

        public override string Interpretar()
        {
            if (_RSI > 50) {
                return "Valor de sobre compra";
            }

            if (_RSI  < 0) {
                return "Nivel Sobre Venta";
            }

            if (_RSI == 0) {
                return "Sin Cambios";
            }

            return "Algo salio mal";
        }

    }
}
